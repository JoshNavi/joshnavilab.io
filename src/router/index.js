import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import GoOutside from '@/components/GoOutside'
import RaceMe from '@/components/RaceMe'
import SupportDashboard from '@/components/SupportDashboard'
import ConsumerLoans from '@/components/ConsumerLoans'
import SiteCMS from '@/components/SiteCMS'
import Waitz from '@/components/Waitz'
import League from '@/components/League'
import FourZeroFour from '@/components/404'

Vue.use(Router)

export default new Router({
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/go-outside',
      name: 'Josh Navi - GO!outside',
      component: GoOutside
    },
    {
      path: '/race-me',
      name: 'Josh Navi - RaceMe',
      component: RaceMe
    },
    {
      path: '/support-dashboard',
      name: 'Josh Navi - Suport Dashboard',
      component: SupportDashboard
    },
    {
      path: '/consumer-loan-app',
      name: 'Josh Navi - Consumer Loan Application',
      component: ConsumerLoans
    },
    {
      path: '/site-cms',
      name: 'Josh Navi - Consumer Loan CMS',
      component: SiteCMS
    },
    {
      path: '/waitz',
      name: 'Josh Navi - Waitz',
      component: Waitz
    },
    {
      path: '/league',
      name: 'League',
      component: League
    },
    {
      path: '*',
      name: '404',
      component: FourZeroFour
    }
  ]
})
